package com.imamfarisi.constructor;

public class Mahasiswa {

    private String nama;
    private Universitas universitas;

    public Mahasiswa(Universitas universitas) {
        System.out.println("=== Inject via Constructor ===");
        this.universitas = universitas;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Universitas getUniversitas() {
        return universitas;
    }

    /* tidak perlu setter krn sudah di inject via constructor
    public void setUniversitas(Universitas universitas) {
        this.universitas = universitas;
    }*/

    @Override
    public String toString() {
        return "Nama : " + nama + ", universitas : " + universitas.getNama();
    }
}
