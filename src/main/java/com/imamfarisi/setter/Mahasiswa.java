package com.imamfarisi.setter;

public class Mahasiswa {

    private String nama;
    private Universitas universitas;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Universitas getUniversitas() {
        return universitas;
    }

    public void setUniversitas(Universitas universitas) {
        System.out.println("=== Inject via Setter ===");
        this.universitas = universitas;
    }

    @Override
    public String toString() {
        return "Nama : " + nama + ", universitas : " + universitas.getNama();
    }
}
