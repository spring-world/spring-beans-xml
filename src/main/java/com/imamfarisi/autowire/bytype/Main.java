package com.imamfarisi.autowire.bytype;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans-autowire-bytype.xml");

        Mahasiswa mahasiswa = context.getBean("mahasiswa", Mahasiswa.class);
        System.out.println(mahasiswa.toString());

        ClassPathXmlApplicationContext cp = (ClassPathXmlApplicationContext) context;
        cp.close();

    }
}
