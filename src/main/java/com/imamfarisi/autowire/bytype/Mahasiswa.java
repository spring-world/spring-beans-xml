package com.imamfarisi.autowire.bytype;

public class Mahasiswa {

    private String nama;
    private Universitas univ;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Universitas getUniv() {
        return univ;
    }

    public void setUniv(Universitas univ) {
        System.out.println("=== Inject via Setter (Autowire byType) ===");
        this.univ = univ;
    }

    @Override
    public String toString() {
        return "Nama : " + nama + ", universitas : " + univ.getNama();
    }
}
