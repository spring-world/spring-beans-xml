package com.imamfarisi.autowire.constructor;

public class Mahasiswa {

    private String nama;
    private Universitas univ;

    public Mahasiswa(Universitas univ) {
        System.out.println("=== Inject via Constructor (Autowire Constructor) ===");
        this.univ = univ;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Universitas getUniv() {
        return univ;
    }

    /* tidak perlu setter krn sudah di inject via constructor
    public void setUniversitas(Universitas universitas) {
        this.universitas = universitas;
    }*/

    @Override
    public String toString() {
        return "Nama : " + nama + ", universitas : " + univ.getNama();
    }
}
