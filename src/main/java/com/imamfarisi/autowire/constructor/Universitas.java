package com.imamfarisi.autowire.constructor;

public class Universitas {

    private String nama;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
