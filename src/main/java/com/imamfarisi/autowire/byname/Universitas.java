package com.imamfarisi.autowire.byname;

public class Universitas {

    private String nama;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
