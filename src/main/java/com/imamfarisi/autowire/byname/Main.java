package com.imamfarisi.autowire.byname;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans-autowire-byname.xml");

        Mahasiswa mahasiswa = context.getBean("mahasiswa", Mahasiswa.class);
        System.out.println(mahasiswa.toString());

        ClassPathXmlApplicationContext cp = (ClassPathXmlApplicationContext) context;
        cp.close();
    }
}
